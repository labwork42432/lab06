﻿using System;
using ClassLibraryMainLabwork4;


namespace ProgramLogicLabwork4
{
    public class PropertyFactory
    {
        public AbstractElectricalAppliance CreatePropertyStandard(string Class, string Name, string Power)
        {
            switch (Class)
            {
                case "HouseholdAppliance":
                    {
                        AbstractElectricalAppliance tmp = new HouseholdAppliance();
                        tmp.Name = Name;
                        tmp.Power = int.Parse(Power);
                        return tmp;
                    }
                case "IndustrialAppliance":
                    {
                        AbstractElectricalAppliance tmp = new IndustrialAppliance();
                        tmp.Name = Name;
                        tmp.Power = int.Parse(Power);
                        return tmp;
                    }
                case "Television":
                    {
                        AbstractElectricalAppliance tmp = new Television();
                        tmp.Name = Name;
                        tmp.Power = int.Parse(Power);
                        return tmp;
                    }
                case "MillingMachine":
                    {
                        AbstractElectricalAppliance tmp = new MillingMachine();
                        tmp.Name = Name;
                        tmp.Power = int.Parse(Power);
                        return tmp;
                    }
                case "LatheMachine":
                    {
                        AbstractElectricalAppliance tmp = new LatheMachine();
                        tmp.Name = Name;
                        tmp.Power = int.Parse(Power);
                        return tmp;
                    }
                case "ElectricArcFurnace":
                    {
                        AbstractElectricalAppliance tmp = new ElectricArcFurnace();
                        tmp.Name = Name;
                        tmp.Power = int.Parse(Power);
                        return tmp;
                    }
                default:
                    {
                        AbstractElectricalAppliance tmp = new HouseholdAppliance();
                        tmp.Name = Name;
                        tmp.Power = int.Parse(Power);
                        return tmp;
                    }
            };     
        }

        public Property CreateProperty()
        {
            HouseholdAppliance Toster = new HouseholdAppliance();
            Toster.Name = "Toast";
            Toster.Power = 100;
            Toster.Size = "32 X 43 X 20";
            Toster.Weight = 5;
            Toster.InterfaceType = "Standart";
            Toster.Colour = "White";
            Toster.PlugType = "Standart";

            Television TV = new Television();
            TV.Name = "TV-5000";
            TV.Power = 70;
            TV.Size = "600 X 150 X 250";
            TV.Weight = 25;
            TV.InterfaceType = "Standart";
            TV.Colour = "White";
            TV.PlugType = "Standart";
            TV.TypeOfScreen = "LCD";
            TV.AmountOfChannels = 40;
            TV.Portabillity = false;

            IndustrialAppliance Rotor = new IndustrialAppliance();
            Rotor.Name = "Rot";
            Rotor.Power = 1000;
            Rotor.Size = "5000 X 5000 X 2000";
            Rotor.Weight = 5000;
            Rotor.NeedOfWorker = false;
            Rotor.ElectronicComponents = false;
            Rotor.BranchOfProduction = "FabricProduction";

            LatheMachine Lat = new LatheMachine();
            Lat.Name = "Lat-500";
            Lat.Power = 500;
            Lat.Size = "2000 X 1000 X 70";
            Lat.Weight = 400;
            Lat.NeedOfWorker = true;
            Lat.ElectronicComponents = true;
            Lat.BranchOfProduction = "MechanicComponentsProduction";
            Lat.TypeOfLathe = "Normal";

            Property property = new Property();

            property.AddAppliance(Toster);
            property.AddAppliance(TV);
            property.AddAppliance(Rotor);
            property.AddAppliance(Lat);

            return property;
        }
    }
    public class PowerCalculator
    {
        public int GetTotalPower(Property property)
        {
            int totalPower = 0;
            foreach (AbstractElectricalAppliance i in property.ElProperty)
            {
                totalPower += i.Power;
            }
            return totalPower;
        }
    }
    public class PropertyPrinter
    {
        public void AppliancePrint(AbstractElectricalAppliance items)
        {
            Console.Write("\nName: " + items.Name + "; \n\tPower: " + items.Power + " \n\tSize: " + items.Size + "; \n\tWeight: " + items.Weight);
        }
        public void PropertyPrint(Property property)
        {
            Console.Write("Property:");
            foreach (AbstractElectricalAppliance i in property.ElProperty)
            {
                AppliancePrint(i);
            }
            PowerCalculator calculator = new PowerCalculator();
            Console.Write("\n\nTotal amount of Power usage: " + calculator.GetTotalPower(property));
        }
    }
}

