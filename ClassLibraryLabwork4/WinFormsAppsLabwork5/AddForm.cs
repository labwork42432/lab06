﻿using System;
using System.Windows.Forms;
using ClassLibraryMainLabwork4;

namespace WinFormsAppsLabwork5
{
    public partial class AddForm : Form
    {
        public DialogResult DResult;
        public string Class;
        public string NameAddForm;
        public string PowerAddForm;
        

        public AddForm()
        {

            InitializeComponent();
            AddClassComboBox.Items.Add("HouseholdAppliance");
            AddClassComboBox.Items.Add("IndustrialAppliance");
            AddClassComboBox.Items.Add("Television");
            AddClassComboBox.Items.Add("MillingMachine");
            AddClassComboBox.Items.Add("LatheMachine");
            AddClassComboBox.Items.Add("ElectricArcFurnace");

            AddPowerTextBox.MaxLength = 7;
        }

        public AddForm(AbstractElectricalAppliance ElApp): this()
        {
            AddNameTextBox.Text = ElApp.Name;
            AddPowerTextBox.Text = ElApp.Power.ToString();
            AddClassComboBox.SelectedItem = ElApp.NameClass();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            DResult = DialogResult.Cancel;
            Close();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if((AddClassComboBox.SelectedItem == null) || 
                (AddNameTextBox.Text == " ") || 
                (AddPowerTextBox.Text == " "))
            {
                MessageForm TempForm = new MessageForm();
                TempForm.MessageFormLabelText= "Not all lines filled";
                TempForm.Show();
            }
            else
            {
                NameAddForm = AddNameTextBox.Text;
                PowerAddForm = AddPowerTextBox.Text;
                Class = AddClassComboBox.SelectedItem.ToString();
                if(int.Parse(PowerAddForm) < 0)
                {
                    MessageForm TempForm = new MessageForm();
                    TempForm.MessageFormLabelText = "Power lower than 0";
                    TempForm.Show();
                }
                else
                {
                    DResult = DialogResult.OK;
                    Close();
                }
            }
        }
    }
}
