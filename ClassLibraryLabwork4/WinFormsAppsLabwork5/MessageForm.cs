﻿using System;
using System.Windows.Forms;

namespace WinFormsAppsLabwork5
{
    public partial class MessageForm : Form
    {
        public MessageForm()
        {
            InitializeComponent();

        }
        public string MessageFormLabelText
        {
            get
            {
                return this.MessageFormLabel.Text;
            }
            set
            {
                this.MessageFormLabel.Text = value;
            }
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
