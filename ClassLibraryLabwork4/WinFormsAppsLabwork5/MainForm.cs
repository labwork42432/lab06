﻿using System;
using System.Windows.Forms;
using ClassLibraryMainLabwork4;
using ProgramLogicLabwork4;

namespace WinFormsAppsLabwork5
{
    public partial class MainForm : Form
    {
        public Property item;

        private void forCalc()
        {
            ItemsListBox.Items.Clear();
            foreach (AbstractElectricalAppliance i in item.ElProperty)
            {
                ItemsListBox.Items.Add(i.Name + " - " + i.Power.ToString() + " W");
            }
            PowerCalculator calculator = new PowerCalculator();
            TotalPowerLabel.Text = "Total power: " + calculator.GetTotalPower(item).ToString() + " W";
        }

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PropertyFactory CountProperty = new PropertyFactory();
            item = CountProperty.CreateProperty();
            forCalc();
            
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddForm TempForm = new AddForm();
            TempForm.ShowDialog();

            if (TempForm.DResult == DialogResult.OK)
            {
                PropertyFactory factory = new PropertyFactory();
                AbstractElectricalAppliance newProp = factory.CreatePropertyStandard(TempForm.Class, TempForm.NameAddForm, TempForm.PowerAddForm);
                item.AddAppliance(newProp);
                forCalc();
            }

        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            if (ItemsListBox.SelectedIndex < 0)
            {
                MessageForm TempForm = new MessageForm();
                TempForm.MessageFormLabelText = "No item have been chosen";
                TempForm.Show();
            }
            else
            {
                AddForm TempForm = new AddForm(item.ElProperty[ItemsListBox.SelectedIndex]);
                TempForm.ShowDialog();
                if (TempForm.DResult == DialogResult.OK)
                {
                    PropertyFactory factory = new PropertyFactory();
                    AbstractElectricalAppliance newProp = factory.CreatePropertyStandard(TempForm.Class, TempForm.NameAddForm, TempForm.PowerAddForm);
                    item.ElProperty[ItemsListBox.SelectedIndex] = newProp;
                    forCalc();
                }
            }

        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (ItemsListBox.SelectedIndex < 0)
            {
                MessageForm TempForm = new MessageForm();
                TempForm.MessageFormLabelText = "No item have been chosen";
                TempForm.Show();
            }
            else
            {
                item.ElProperty.RemoveAt(ItemsListBox.SelectedIndex);
                forCalc();
            }

        }
    }
}
