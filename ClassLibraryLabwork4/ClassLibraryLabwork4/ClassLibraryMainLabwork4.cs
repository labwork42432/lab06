﻿using System;
using System.Collections.Generic;

namespace ClassLibraryMainLabwork4
{
    public abstract class AbstractElectricalAppliance
    {
        private string name;
        private int power;
        private string size;
        private int weight;

        public abstract string NameClass();


        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public int Power
        {
            get { return power; }
            set { power = value; }
        }
        public string Size
        {
            get { return size; }
            set { size = value; }
        }
        public int Weight
        {
            get { return weight; }
            set { weight = value; }
        }
    }

    public class HouseholdAppliance : AbstractElectricalAppliance
    {
        private string interfaceType;
        private string colour;
        private string plugType;

        public override string NameClass()
        {
            return ("HouseholdAppliance");
        }

        public string InterfaceType
        {
            get { return interfaceType; }
            set { interfaceType = value; }
        }
        public string Colour
        {
            get { return colour; }
            set { colour = value; }
        }
        public string PlugType
        {
            get { return plugType; }
            set { plugType = value; }
        }
    }

    public class IndustrialAppliance : AbstractElectricalAppliance
    {
        private bool needOfWorker;
        private bool electronicComponents;
        private string branchOfProduction;

        public override string NameClass()
        {
            return ("IndustrialAppliance");
        }

        public bool NeedOfWorker
        {
            get { return needOfWorker; }
            set { needOfWorker = value; }
        }
        public bool ElectronicComponents
        {
            get { return electronicComponents; }
            set { electronicComponents = value; }
        }
        public string BranchOfProduction
        {
            get { return branchOfProduction; }
            set { branchOfProduction = value; }
        }
    }
    public class Television : HouseholdAppliance
    {
        private string typeOfScreen;
        private int amountOfChannels;
        private bool portabillity;

        public override string NameClass()
        {
            return ("Television");
        }

        public string TypeOfScreen
        {
            get { return typeOfScreen; }
            set { typeOfScreen = value; }
        }
        public int AmountOfChannels
        {
            get { return amountOfChannels; }
            set { amountOfChannels = value; }
        }
        public bool Portabillity
        {
            get { return portabillity; }
            set { portabillity = value; }
        }
    }

    public class MillingMachine : IndustrialAppliance
    {
        private string typeOfMilling;

        public override string NameClass()
        {
            return ("MillingMachine");
        }

        public string TypeOfMilling
        {
            get { return typeOfMilling; }
            set { typeOfMilling = value; }
        }
    }
    public class LatheMachine : IndustrialAppliance
    {
        private string typeOfLathe;

        public override string NameClass()
        {
            return ("LatheMachine");
        }

        public string TypeOfLathe
        {
            get { return typeOfLathe; }
            set { typeOfLathe = value; }
        }
    }
    public class ElectricArcFurnace : IndustrialAppliance
    {
        private string typeOfArcFurnace;

        public override string NameClass()
        {
            return ("ElectricArcFurnace");
        }

        public string TypeOfArcFurnace
        {
            get { return typeOfArcFurnace; }
            set { typeOfArcFurnace = value; }
        }
    }

    public class Property
    {
        private List<AbstractElectricalAppliance> property = new List<AbstractElectricalAppliance>();

        public List<AbstractElectricalAppliance> ElProperty

        {
            get { return property; }

            set { property = value; }
        }
        public void AddAppliance(AbstractElectricalAppliance abstractElectricalAppliance)
        {
            property.Add(abstractElectricalAppliance);
        }
    }
}
