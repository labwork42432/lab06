﻿using System;
using ClassLibraryMainLabwork4;


namespace LogicLabwork4
{
    public class PropertyFactory
    {
        public Property CreateProperty()
        {
            HouseholdAppliance Toster = new HouseholdAppliance();
            Toster.Name = "Toast";
            Toster.Power = 100;
            Toster.Size = "32 X 43 X 20";
            Toster.Weight = 5;
            Toster.InterfaceType = "Standart";
            Toster.Colour = "White";
            Toster.PlugType = "Standart";

            Television TV = new Television();
            TV.Name = "TV-5000";
            TV.Power = 70;
            TV.Size = "600 X 150 X 250";
            TV.Weight = 25;
            TV.InterfaceType = "Standart";
            TV.Colour = "White";
            TV.PlugType = "Standart";
            TV.TypeOfScreen = "LCD";
            TV.AmountOfChannels = 40;
            TV.Portabillity = false;

            IndustrialAppliance Rotor = new IndustrialAppliance();
            Rotor.Name = "Rot";
            Rotor.Power = 1000;
            Rotor.Size = "5000 X 5000 X 2000";
            Rotor.Weight = 5000;
            Rotor.NeedOfWorker = false;
            Rotor.ElectronicComponents = false;
            Rotor.BranchOfProduction = "FabricProduction";

            LatheMachine Lat = new LatheMachine();
            Lat.Name = "Lat-500";
            Lat.Power = 500;
            Lat.Size = "2000 X 1000 X 70";
            Lat.Weight = 400;
            Lat.NeedOfWorker = true;
            Lat.ElectronicComponents = true;
            Lat.BranchOfProduction = "MechanicComponentsProduction";
            Lat.TypeOfLathe = "Normal";

            Property property = new Property();

            property.AddAppliance(Toster);
            property.AddAppliance(TV);
            property.AddAppliance(Rotor);
            property.AddAppliance(Lat);

            return property;
        }
    }
    public class PowerCalculator
    {
        public int GetTotalPower(Property property)
        {
            int totalPower = 0;
            foreach (AbstractElectricalAppliance i in property.ElProperty)
            {
                totalPower += i.Power;
            }
            return totalPower;
        }
    }
    public class PropertyPrinter
    {
        public void AppliancePrint(AbstractElectricalAppliance items)
        {
            Console.Write("\tName: " + items.Name + "; Power: " + items.Power + " Size: " + items.Size + "; Weight: " + items.Weight);
        }
        public void PropertyPrint(Property property)
        {
            Console.Write("Property:");
            foreach (AbstractElectricalAppliance i in property.ElProperty)
            {
                AppliancePrint(i);
            }
            PowerCalculator calculator = new PowerCalculator();
            Console.Write("Total amount of Power usage: "+ calculator.GetTotalPower(property));
        }
        

    }
}
