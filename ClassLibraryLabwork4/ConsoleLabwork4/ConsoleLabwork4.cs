﻿using System;
using ClassLibraryMainLabwork4;
using ProgramLogicLabwork4;

namespace ConsoleLabwork4
{
    class ConsoleLabwork4
    {
        static void Main(string[] args)
        {
            PropertyFactory CountProperty = new PropertyFactory();
            Property property = CountProperty.CreateProperty();
            PropertyPrinter propertyPrinter = new PropertyPrinter();
            propertyPrinter.PropertyPrint(property);

            Console.ReadKey();
        }
    }
}
