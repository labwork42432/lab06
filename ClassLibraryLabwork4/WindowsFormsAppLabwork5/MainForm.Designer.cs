﻿
namespace WindowsFormsAppLabwork5
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ItemsListBox = new System.Windows.Forms.ListBox();
            this.AddElementButton = new System.Windows.Forms.Button();
            this.EditElementButton = new System.Windows.Forms.Button();
            this.DeleteElementButton = new System.Windows.Forms.Button();
            this.TotalPowerLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ItemsListBox
            // 
            this.ItemsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ItemsListBox.FormattingEnabled = true;
            this.ItemsListBox.ItemHeight = 25;
            this.ItemsListBox.Location = new System.Drawing.Point(203, 12);
            this.ItemsListBox.Name = "ItemsListBox";
            this.ItemsListBox.Size = new System.Drawing.Size(286, 379);
            this.ItemsListBox.TabIndex = 0;
            // 
            // AddElementButton
            // 
            this.AddElementButton.Location = new System.Drawing.Point(23, 73);
            this.AddElementButton.Name = "AddElementButton";
            this.AddElementButton.Size = new System.Drawing.Size(159, 43);
            this.AddElementButton.TabIndex = 1;
            this.AddElementButton.Text = "Add";
            this.AddElementButton.UseVisualStyleBackColor = true;
            this.AddElementButton.Click += new System.EventHandler(this.AddElementButton_Click);
            // 
            // EditElementButton
            // 
            this.EditElementButton.Location = new System.Drawing.Point(23, 149);
            this.EditElementButton.Name = "EditElementButton";
            this.EditElementButton.Size = new System.Drawing.Size(159, 43);
            this.EditElementButton.TabIndex = 2;
            this.EditElementButton.Text = "Edit";
            this.EditElementButton.UseVisualStyleBackColor = true;
            this.EditElementButton.Click += new System.EventHandler(this.EditElementButton_Click);
            // 
            // DeleteElementButton
            // 
            this.DeleteElementButton.Location = new System.Drawing.Point(23, 232);
            this.DeleteElementButton.Name = "DeleteElementButton";
            this.DeleteElementButton.Size = new System.Drawing.Size(159, 43);
            this.DeleteElementButton.TabIndex = 3;
            this.DeleteElementButton.Text = "Delete";
            this.DeleteElementButton.UseVisualStyleBackColor = true;
            this.DeleteElementButton.Click += new System.EventHandler(this.DeleteElementButton_Click);
            // 
            // TotalPowerLabel
            // 
            this.TotalPowerLabel.AutoSize = true;
            this.TotalPowerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TotalPowerLabel.Location = new System.Drawing.Point(212, 409);
            this.TotalPowerLabel.Name = "TotalPowerLabel";
            this.TotalPowerLabel.Size = new System.Drawing.Size(122, 25);
            this.TotalPowerLabel.TabIndex = 4;
            this.TotalPowerLabel.Text = "Total Power:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(512, 471);
            this.Controls.Add(this.TotalPowerLabel);
            this.Controls.Add(this.DeleteElementButton);
            this.Controls.Add(this.EditElementButton);
            this.Controls.Add(this.AddElementButton);
            this.Controls.Add(this.ItemsListBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox ItemsListBox;
        private System.Windows.Forms.Button AddElementButton;
        private System.Windows.Forms.Button EditElementButton;
        private System.Windows.Forms.Button DeleteElementButton;
        private System.Windows.Forms.Label TotalPowerLabel;
    }
}

